<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Helpers\MessageHelper;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function test() {
        dd("here");
    }

    /**
     * Send control back with exception
     * message
     * @param type $ex
     * @param type $inputs
     * @return type
     */
    public function sendBackWithException($ex, $inputs = []) {
        return redirect()->back()->withInput($inputs)
                        ->with("message", "danger=" . $ex->getMessage());
    }

    /**
     * Send conrol back with error
     * message
     * @param type $ex
     * @param type $inputs
     * @return type
     */
    public function backWithError($error = "", $inputs = []) {
        return redirect()->back()->withInput($inputs)
                        ->with("message", "danger=" . $this->checkErrorString($error));
    }

    /**
     * Go to route with error
     * @param type $route
     * @param type $error
     * @return type
     */
    public function goWithError($route, $error = "") {
        return redirect()->route($route)->with("message", "danger=" . $this->checkErrorString($error));
    }

    /**
     * Go to route with error
     * @param type $route
     * @param type $error
     * @return type
     */
    public function goWithSuccess($route, $message = "") {
        return redirect()->route($route)->with("message", "success=$message");
    }

    public function showException($exceptionMessage) {
        return view("exceptions.exception", compact("exceptionMessage"));
    }

    /**
     * Check error if empty then send default error message
     * @param type $error
     * @return type
     */
    public function checkErrorString($error) {
        if (empty($error)) {
            $error = MessageHelper::$error["generalError"];
        }
        return $error;
    }

}
