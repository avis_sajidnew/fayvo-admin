<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function loginPage() {
        return view('admin.login');
    }

    public function postLogin(Request $request, $error ="",$success = "") {
        if (\Auth::attempt($request->except("_token"))) {
            if (empty($success)) {
                    $success = \App\Helpers\MessageHelper::$success["loginSuccess"];
                }
//                return \Redirect()->route('locations')->with("message", "danger=" . $error);
            return \redirect()->to("admin/dashboard")->with('message','success='. $success);
        }
        if(empty($error)){
            $error = \App\Helpers\MessageHelper::$error['invalidLoginInput'];
        }
        return redirect()->back()->with("message", "danger=".$error);
    }

    public function logout() {
        \Session::flush();
        return redirect()->to("admin/login");
    }

    /*
     * Method is for total counters of dashboard
     */

    public function dashboard() {
        $date = [];
        $data = 1;
        $friends = \App\Friends::total_friend_requests($date, $data);
        $comment = \App\Comments::comment_counter_with_date($date, $data);
        $likes = \App\Likes::likes_counter_with_date($date, $data);
        $posts = \App\Posts::post_counter($date, $data);
        $messages = \App\Messages::message_count_with_date($date, $data);
        $users = \App\User::user_counter_with_date($date, $data);
        $notifications = \App\FayvoActivities::total_active_notifications($date, $data);
        $remove_accounts = \App\RemoveUserAccount::total_remove_accounts($date, $data);

        return View("admin/dashboard/dashboard", compact('friends', 'comment', 'likes', 'posts', 'messages', 'users', 'notifications', 'remove_accounts'));
    }

    /*
     * Static Counter detail function
     */

    public function reports() {
        $data = Input::except('_token');
        $date = [];
        if (isset($data['to'])) {
            $date['to'] = $data['to'];
        }
        if (isset($data['from'])) {
            $date['from'] = $data['from'];
        }
        $remove_accounts = \App\RemoveUserAccount::total_remove_accounts($date);
        $comment = \App\Comments::comment_counter_with_date($date);
        $likes = \App\Likes::likes_counter_with_date($date);
        $posts = \App\Posts::post_counter($date);
        $messages = \App\Messages::message_count_with_date($date);
        $users = \App\User::user_counter_with_date($date);
        $notifications = \App\FayvoActivities::total_active_notifications($date);
        $friends = \App\Friends::total_friend_requests($date);

        return view('admin/dashboard/reports', compact('friends', 'comment', 'likes', 'posts', 'messages', 'users', 'notifications', 'remove_accounts'));
    }

    /*
     * method for total user recomendations
     */

    public function recomendations() {

        $recomendations = \App\UserSuggestion::get_all_recomendations();
        return View("admin/dashboard/user_recomendations", compact('recomendations'));
    }

    /*
     * method is for large recomendation messages
     */

    public function view_recomendation($id) {

        $recomendations = \App\UserSuggestion::get_recomendation_by_id($id);
        return View("admin/dashboard/readmore", compact('recomendations'));
    }

   
    
}
