<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard() {
        return view('template_views.dashboard');
    }

    public function user_profile($id) {

        $user = User::getUserData($id);
        $followers = $user->followers()->paginate(5);
        $box = User::getUserbox($id);
//        echo '<pre>';        dd($box);die;
        $following = $user->following()->paginate(5);
        return view('template_views.user_profile', compact('user', 'followers', 'following'));
    }

    public function loginPage() {
        return view('template_views.login');
    }

    public function postLogin(Request $request) {
        if (\Auth::attempt($request->except("_token"))) {
            return \Redirect::to("admin/dashboard");
        }
        return redirect()->back()->with("message", "Login failed");
    }

    public function logout() {
        \Session::flush();
        return redirect("admin/login");
    }

    public function listing() {
        $data = User::get_user_list();
//        dd($data);
        return \View::make('template_views.listing')->with('data', $data);
    }
    
    /**
     * API's
     */ 
    public function getIbooksAPIterm() {
        $API = file_get_contents("https://itunes.apple.com/lookup?id=356883079"); 
        return $API;
    }
    /*
     * Static login detail function
     */
    public function static_login() {
//        dd("here");
//        $data = User::get_user_list();
//        $total = User::getUserlogged()->paginate(12);
//        dd($data);
        return view('template_views.static_login');
    }
    public function datePost() {
//        dd($_POST);
        $inputs = Input::except('_token');
        
//        $data = User::get_user_list();
//        $total = User::getUserlogged($inputs);
        $total = \App\UserLoginStatus::getLogInCountWithUser($inputs);
//        dd($total);
        return view('template_views.static_login')->with('data', $total);
    }
    
}
