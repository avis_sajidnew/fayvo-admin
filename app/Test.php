<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    
    
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function followers() {

        return $this->hasMany("App\Friends", "following_id", "id")->where("status", "=", 'A');
    }

    public function following() {

        return $this->hasMany("App\Friends", "follower_id", "id")->where("status", "=", "A");
    }
    public function get_boxes(){
        return $this->hasMany("App\Box","user_id","id");
    }
    static function getUserData($id) {
        return  User::where("id", "=", $id)->first();
    }

    static function get_following($id) {
        $following = User::where("id", "=", $id)->with(['following' => function ($sql) use ($id) {
                        $sql->where('following_id', $id);
                    }])->get();
        return $following;
    }

    static function get_user_list() {
        $data = User::where('role_id', '=', '2')->paginate(2);
        return $data;
    }
    
    public function get_Logged_users() {

        return $this->hasMany("App\User_Login_Status", "user_id", "id")->where("is_login", "=", '1');
    }
//    static function getUserbox($id) {
//        SELECT @last_to := MAX(message_from) FROM messages 
//        SELECT MAX(id) FROM messages WHERE message_to = 8215
//        SELECT MAX(id), MAX(message_from) , MAX(message_to) FROM messages
//        	
//2019-01-17 12:50:34 2017-12-26 17:26:10
//SELECT *
//
//SELECT * FROM `user_login_status` WHERE created_at BETWEEN '2017-12-26 17:26:10' 
//AND '2017-12-27 17:26:10' AND is_login = '1'
//
//SELECT  count(user_id) as user_id FROM `user_login_status` WHERE created_at BETWEEN '2017-12-26 17:26:10' 
//AND '2017-12-27 17:26:10' AND is_login = '1' GROUP BY user_id
//right query
//SELECT count(*) as total_login,user_id FROM `user_login_status` where is_login='1' GROUP  by user_id
//        return  User::where("id","=",$id)->with(['get_boxes'=> function ($sql) use ($id){
//            $sql->where('id',$id);
//        }])->get();
//    }
    static function getUserbox($id) {
        $following = User::where("id", "=", $id)->with(['get_boxes' => function ($sql) use ($id) {
                        $sql->where('user_id', $id);
                    }])->get();
        return $following;
    }
     static function getUserlogged($inputs) {
         
//          echo '<pre>';
//         print_r($inputs);
//         die;
//         $time = strtotime('Y-m-d', date());
//         echo '<pre>';
//         print_r($time);
//         die;
        $data = User::where('role_id', '=', '2')->with(['get_Logged_users' => function ($sql)  {
//            $sql->where('created_at' ,'=', $time);
                    }])->paginate(12);
//        echo '<pre>';
//         print_r($data->toarray());
//         die;
                    return $data;
    }
}
