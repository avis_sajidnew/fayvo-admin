<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model {

    protected $table = 'user_posts';

    /*
     * Method for count tolal posts with date and without date
     */
    public static function post_counter($date, $dashboard = null) {
        $data = Posts::where(function($sql) use($date, $dashboard) {
                    if ($dashboard == null) {
                        if (!empty($date)) {
                            $sql->whereDate("created_at", ">=", $date['from']);
                            if (!empty($date['to'])) {
                                $sql->whereDate('created_at', '<=', $date['to']);
                            }
                        }
                    }
                })->count();
        return $data;
    }

}
