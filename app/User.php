<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*
     * Method for count tolal users with date and without date
     */

    public static function user_counter_with_date($date, $dashboard = null) {

        $data = User::where(function($sql) use($date, $dashboard) {
                    if ($dashboard == null) {
                        if (!empty($date)) {
                            $sql->whereDate("created_at", ">=", $date['from']);
                            if (!empty($date['to'])) {
                                $sql->whereDate('created_at', '<=', $date['to']);
                            }
                        }
                    }
                })->count();
        return $data;
    }

    /*
     * method for get all users
     */

    public static function get_all_users($search = null) {
        $data = User::where('role_id', '=', 2)
                        ->where('archive', '=', false)
                        ->where(function ($sql) use ($search) {
                            if (!empty($search)) {
                                $sql->where('full_name', 'like', "%$search%");
                                $sql->orWhere('username', 'like', "%$search%");
                                $sql->orWhere('email', 'like', "%$search%");
                                $sql->orWhere('phone', 'like', "%$search%");
                            }
                        })->orderBy('id', 'DESC')->paginate(50);
        return $data;
    }

}
