<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Messages extends Model {

    protected $table = 'messages';
    protected $fillable = [
        'id', 'user_id', 'name', 'created_at', 'status'
    ];

    /*
     * Method for count tolal messages with date and without date
     */
    public static function message_count_with_date($date, $dashboard = null) {

        $data = Messages::where(function($sql) use($date, $dashboard) {
                    if ($dashboard == null) {
                        if (!empty($date)) {
                            $sql->whereDate("created_at", ">=", $date['from']);
                            if (!empty($date['to'])) {
                                $sql->whereDate('created_at', '<=', $date['to']);
                            }
                        }
                    }
                })->count();
        return $data;
    }

}
