<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model {

    protected $table = 'likes';
    protected $fillable = [
        'id', 'liked_to', 'liked_by', 'created_at',
    ];

    /*
     * Method for count tolal likes with date and without date
     */
    public static function likes_counter_with_date($date, $dashboard = null) {

        $data = Likes::where(function($sql) use($date, $dashboard) {
                    if ($dashboard == null) {
                        if (!empty($date)) {
                            $sql->whereDate("created_at", ">=", $date['from']);
                            if (!empty($date['to'])) {
                                $sql->whereDate('created_at', '<=', $date['to']);
                            }
                        }
                    }
                })->count();
        return $data;
    }

}
