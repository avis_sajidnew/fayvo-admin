<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friends extends Model {

    protected $table = 'friends';
    
    /*
     * Method for count tolal friends requests with date and without date
     */
    public static function total_friend_requests($date, $dashboard = null) {
         
            $data = Friends::where(function($sql) use($date,$dashboard) {
                if ($dashboard == null) {
                if (!empty($date)) {
                $sql->where('status','=','P');
                        $sql->whereDate("created_at", ">=", $date['from']);
                        if (!empty($date['to'])) {
                            $sql->whereDate('created_at', '<=', $date['to']);
                        }
                    }
                }
                    })->count();
            return $data;
        }
    

}
