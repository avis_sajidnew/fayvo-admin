<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLoginStatus extends Model {

    protected $table = 'user_login_status';
    protected $fillable = [
        'id', 'user_id', 'is_login', 'created_at',
    ];

    public function user() {
        return $this->hasOne("App\User", "id", "user_id");
    }

    public static function getLogInCountWithUser($inputs) {
    if(!empty($inputs['created_at'])){
        $users = static::select(\DB::raw("count(id) as total_login,user_id"))
                        ->where(function($sql) use($inputs) {
                            $sql->where("is_login", "=", true);
                            $sql->whereDate("created_at", ">=", $inputs['created_at'] . ' 00:00:00');
                            if(!empty($inputs['created_at_end'])){
                            $sql->whereDate("created_at", "<=", $inputs['created_at_end'] . ' 00:00:00');
                            }
                        })->whereHas("user")->with(["user" => function($sql) {
                        $sql->select("id", "username");
                    }])->groupBy("user_id")->paginate(10);
        return !$users->isEmpty() ? $users : [];
    }
 else {
//        return redirect('datePost');
     return false;
        
    }
    }
    static function get_user_list($type = []) {
        
//        dd($type);
        $data = User::where('role_id', '=', '2');
                if(isset($type['public']) && $type['public'] == "0"){
             $data->where('is_live','=','1');
                }
                 if(isset($type['private']) && $type['private'] == "1"){
             $data->where('is_live','=','0');
                 }
             $data->withCount('get_comments')
                ->withCount('get_likes')->paginate(12);
        return $data;
    }
    
    

}
