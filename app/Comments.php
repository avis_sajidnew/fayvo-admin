<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model {

    protected $table = 'post_comments';

    /*
     * Method for count tolal Comments with date and without date
     */
    public static function comment_counter_with_date($date, $dashboard = null) {

        $data = Comments::where(function($sql) use($date, $dashboard) {
                    if ($dashboard == null) {
                        if (!empty($date)) {
                            $sql->whereDate("created_at", ">=", $date['from']);
                            if (!empty($date['to'])) {
                                $sql->whereDate('created_at', '<=', $date['to']);
                            }
                        }
                    }
                })->count();
        return $data;
    }

}
