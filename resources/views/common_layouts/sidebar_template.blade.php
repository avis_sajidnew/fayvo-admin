<div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">                    
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start active open ">
                            <a href="{{ url('admin/dashboard') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{{url('admin/users')}}" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Users</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="{{url('admin/static-login')}}" class="nav-link nav-toggle">
                                <i class="icon-docs"></i>
                                <span class="title">Static Login</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('admin/conversation')}}" class="nav-link nav-toggle">
                                <i class="icon-grid"></i>
                                <span class="title">Conversation</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="user_reports.html" class="nav-link nav-toggle">
                                <i class="icon-speech"></i>
                                <span class="title">Reports / Complaints</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>
                        <li class="nav-item  ">
                            <a href="user-recomendations.html" class="nav-link nav-toggle">
                                <i class="icon-like"></i>
                                <span class="title">User Recomendations</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="system_boxes.html" class="nav-link nav-toggle">
                                <i class="icon-grid"></i>
                                <span class="title">System Boxes</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="delete_ac_requests.html" class="nav-link nav-toggle">
                                <i class="icon-trash"></i>
                                <span class="title">Delete Account Requests</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="marketing.html" class="nav-link nav-toggle">
                                <i class="icon-rocket"></i>
                                <span class="title">Fayvo Marketing</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="trending.html" class="nav-link nav-toggle">
                                <i class="icon-feed"></i>
                                <span class="title">Trendings</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END SIDEBAR -->
            </div>