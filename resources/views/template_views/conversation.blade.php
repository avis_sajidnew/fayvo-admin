<?php
//echo "<pre>";
//        print_r($data->toArray());
//        echo "</pre>";
//        die;     
?>
@extends('common_layouts.main_layouts')
@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <h1 class="page-title"> Users 
                <small>/Conversation</small>
            </h1>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>/Conversation</span>
                        <i class="fa fa-angle-right"></i>
                    </li>
                </ul>

            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-users font-dark"></i>
                                <span class="caption-subject bold uppercase">All Conversations</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                <thead>
                                    <tr class="">
                                        <th> Channel </th>
                                        <th> Sender Name </th>
                                        <th> Receiver Name </th>
                                        <th> Total Messages </th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $users)
                                    <?php //echo '<pre>';print_r($users->toArray()['message_from']['username']);exit; ?>
                                    <tr>
                                        <td> {{ $users->chat_channel }} </td>
                                        <td> {{ $users->toArray()['message_from']['username'] }} </td>
                                        <td> {{ $users->toArray()['message_to']['username'] }}</td>
                                        <td> {{ $users->total_messages }} </td>
                                        <td>
                                           
                                            <a href="{{ url('/admin/chat_content/' . $users->chat_channel) }}"><span class="label label-success margin-right-10"> View </span></a>
                                        </td>
                                    </tr>
                                    @endforeach  
                                </tbody>

                            </table>
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->

    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
    <!--[if lt IE 9]>
<script src="{{asset('assets')}}/global/plugins/respond.min.js"></script>
<script src="{{asset('assets')}}/global/plugins/excanvas.min.js"></script> 
<![endif]-->
    <!-- BEGIN CORE PLUGINS -->
<!--        <script src="{{asset('assets')}}/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
     END CORE PLUGINS 
     BEGIN PAGE LEVEL PLUGINS 
    <script src="{{asset('assets')}}/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
     END PAGE LEVEL PLUGINS 
     BEGIN THEME GLOBAL SCRIPTS 
    <script src="{{asset('assets')}}/global/scripts/app.min.js" type="text/javascript"></script>
     END THEME GLOBAL SCRIPTS 
     BEGIN PAGE LEVEL SCRIPTS 
    <script src="{{asset('assets')}}/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>
     END PAGE LEVEL SCRIPTS 
     BEGIN THEME LAYOUT SCRIPTS 
    <script src="{{asset('assets')}}/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>-->
    <!-- END THEME LAYOUT SCRIPTS -->
    @stop
