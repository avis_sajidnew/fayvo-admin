@extends('common_layouts.main_layouts')



@section('content')
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN THEME PANEL -->
                <!-- END THEME PANEL -->
                <h1 class="page-title"> CSV
                    <!--<small>All posts lists</small>-->
                </h1>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.html">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <span>CSV</span>
                        </li>
                    </ul>
                    <div class="page-toolbar">
                        <div class="btn-group pull-right filterz ">
                            <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> 
                                <i class="fa fa-filter filico"></i>

                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>
                                    <a href="all_posts.html">
                                        <i class="icon-layers"></i> View All Posts</a>
                                </li>
                                <li>
                                    <a href="archived_posts.html">
                                        <i class="icon-shield"></i> All Archived Posts</a>
                                </li>
                                <li>
                                    <a href="trending_posts_owner.html">
                                        <i class="fa fa-cubes"></i> Trending Posts Users</a>
                                </li>
                                <li>
                                    <a href="continent_posts.html">
                                        <i class="icon-map"></i> Trending Posts Map</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light ">
                            <div class="portlet-title">
                                @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                                @endif
                                <div class="caption font-dark">
                                    <i class="icon-users font-dark"></i>
                                    <span class="caption-subject bold uppercase">CSV</span>
                                </div>

                                <form action="{{ url('admin/download')}}" class="form-horizontal" method="POST" >
                                    {{ csrf_field() }}
                                    <div class="Src_to_from">
                                        <div class="">
                                            <label class="control-label mt5 pull-left mr10">Search Results</label>
                                            <div class="input-group date-picker input-daterange" data-date="2012-06-22" data-date-format="yyyy-mm-dd">
                                                <input type="text" class="form-control" name="from"  id="from" placeholder="From">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </div>

                                            <div class="input-group date-picker input-daterange" data-date="2012-06-22" data-date-format="yyyy-mm-dd">
                                                <input type="text" class="form-control" name="to" placeholder="To">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </div>
                                            <!--<a href="http://www.highcharts.com/" class="btn green btn-outline ml15" target="_blank">Search</a>-->
                                            <button type="submit" class="btn green btn-outline ml15" target="_blank">Download</button

                                        </div>
                                    </div><!--Src_to_from-->

                            </div>
                            <div class="portlet-body">

                                <!-- END PAGE HEADER-->
                                <div class="portfolio-content portfolio-1 a_ll_posts">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                            <div class="portlet light ">

                                                <div class="portlet-body">

                                                    <div class="form-body">
                                                        <div class="bxposts w100">
                                                            <div class="Range w100">
                                                                <div class="md-radio-list">
                                                                    <div class="bxposts w100">
                                                                        <div class="Range w100">
                                                                            <div class="md-radio-list">
                                                                                <div class="md-radio">

                                                                                    <input type="checkbox" name="user_registers" value="3"> Total User Registers <br>

                                                                                </div>
                                                                                <div class="md-radio">

                                                                                    <input type="checkbox" name="posts" value="3"> Total Posts<br>

                                                                                </div>
                                                                                <div class="md-radio">

                                                                                    <input type="checkbox" name="comments" value="3"> Total Comments<br>

                                                                                </div>
                                                                                <div class="md-radio">

                                                                                    <input type="checkbox" name="likes" value="3"> Likes<br>

                                                                                </div>
                                                                                <div class="md-radio">
                                                                                    <input type="checkbox" name="followers" value="3"> Followers<br>
                                                                                </div>
                                                                                <div class="md-radio">
                                                                                    <input type="checkbox" name="following" value="3"> Following<br>
                                                                                </div>
                                                                                

                                                                            </div>
                                                                        </div><!--boxesList-->
                                                                    </div>

                                                                </div>
                                                            </div><!--boxesList-->
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <!--<button type="submit" class="btn green">Send to All</button>-->

                                                            </div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>                                
                                            </div>

                                        </div>

                                    </div>
                                    <div id="js-grid-juicy-projects" class="cbp">

                                    </div>
                                </div>
                            </div>

                            <!-- END CONTENT BODY -->
                        </div><!--portlet-body-->
                    </div>

                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<script src="link">

</script>
@stop