<?php
//echo "<pre>";
//        print_r($data->toArray());
//        echo "</pre>";
//        die;     
?>
@extends('common_layouts.main_layouts')
@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <h1 class="page-title"> Users 
                <small>/Conversation/Message Content</small>
            </h1>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>/Conversation</span>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>/Message Content</span>
                        <i class="fa fa-angle-right"></i>
                    </li>
                </ul>

            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-users font-dark"></i>
                                <span class="caption-subject bold uppercase"> Conversation Detail</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                <thead>
                                    <tr class="">
                                        <th> ID# </th>
                                        <th> Message Recive From </th>
                                        <th> Message Send To </th>
                                        <th> Chat Messaging </th>
                                        <th> Status </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $users)
                                    <tr>
                                        <td> {{ $users->id}} </td>
                                        <td> {{ $users->toArray()['message_from']['username']}} </td>
                                        <td> {{ $users->toArray()['message_to']['username'] }}</td>
                                        <td> {{ base64_decode($users->message_content)}} </td>
                                        @if($users->toArray()['status'] == 'S')         
                                        <td>Sent </td>
                                        @elseif($users->toArray()['status'] == 'D')
                                        <td>Deliver</td> 
                                        @elseif($users->toArray()['status'] == 'V')
                                        <td>Viewed</td>  
                                        @endif
                                    </tr>
                                    @endforeach  
                                </tbody>
                            </table>
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->

    <!-- END QUICK SIDEBAR -->
</div>

@stop
