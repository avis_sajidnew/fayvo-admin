<?php// echo '<pre>'; print_r($data->toArray());die;              ?>
@extends('common_layouts.main_layouts')
@section('content')

<html lang="en">

    <style>
        label {
            display: block;
            font: 1rem 'Fira Sans', sans-serif;
        }

        input,
        label {
            margin: .4rem 0;
        }

        .note {
            font-size: .8em;
        }

    </style>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    <!-- END THEME PANEL -->
                    <h1 class="page-title"> Users 
                        <small>Follow / Following</small>
                    </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.html">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Follow / Following</span>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ul>
                        <div class="page-toolbar dn">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="#">
                                            <i class="icon-bell"></i> Action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-shield"></i> Another action</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-user"></i> Something else here</a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#">
                                            <i class="icon-bag"></i> Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-users font-dark"></i>
                                        <span class="caption-subject bold uppercase">All Logged Users Detail</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                        <form action="{{url('admin/datePost')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="start">Start date:</label>
                                                    <input type="date" id="created_at" name="created_at"
                                                           value="2018-07-22"
                                                           >
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="start">End date:</label>
                                                    <input type="date" id="start" name="created_at_end"
                                                           value="2018-07-22"
                                                           >
                                                    <button type="submit" class="btn btn-fit-height grey-salt data-delay="1000" data-close-others="true"> Search
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <thead>
                                            <tr class="">
                                                <th> ID </th>
                                                <th> Username </th>
                                                <th> Login Count </th>
                                            </tr>
                                        </thead>
                                        <?php if(!empty($data)) {?>
                                         @foreach($data as $dataa)
                                                    <tbody>
                                                        <tr>
                                                            <td> {{ $dataa->user_id }} </td>
                                                            
                                                            <td> {{ $dataa->user['username'] }} </td>
                                                             <td> {{ $dataa->total_login }} </td>

                                                        </tr>
                                                    </tbody>
                                                    @endforeach
                                                </table>
                                    {{ $data->links() }}
                                        <?php }?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>

            @stop
