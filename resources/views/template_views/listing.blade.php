<?php 
//echo "<pre>";
//        print_r($data->toArray());
//        echo "</pre>";
//        die;     
        ?>
@extends('common_layouts.main_layouts')
@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <h1 class="page-title"> Dashboard 
                <small>View all Users</small>
            </h1>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Users</span>
                    </li>
                </ul>
               
            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-users font-dark"></i>
                                <span class="caption-subject bold uppercase">All Users List</span>
                            </div>
                            <div class="actions w75">
                                <div class="table-group-actions pull-right">
                                    <form method="post" action="{{url('admin/search')}}">
                                        {{ csrf_field() }}
                                    <div class="search-bar bordered pull-right">
                                        <div class="pull-right w50 ml10">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="search" placeholder="Search for Users...">
                                                <span class="input-group-btn">
                                                    <button class="btn green-soft uppercase bold" type="submit">Search</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <div class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default">
                                                    <input type="radio" class="toggle" id="private" name="private" value="1"> Private </label>
                                                <label class="btn btn-default ">
                                                    <input type="radio" class="toggle" id="public" name="public" value="0"> Public </label>
                                                
                                                    <input type="checkbox" class="toggle" id="comment" name="comment" value="1"> Comments </label>
                                                
                                                    <input type="checkbox" class="toggle" id="like" name="like" value="1"> Likes </label>
                                            </div>
                                        </div>
                                    </div>
                                        </form>
                                </div>
                            </div>
                        </div>	
                        @foreach($data  as $users)
                        <div class="portlet-body">
                            <div class="panel-group accordion UzerAccord" id="accordion1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#" href="#" aria-expanded="false">
                                                <p class="muted">

                                                    <br/>

                                                    <img src="{{asset('assets')}}/pages/img/avatars/team7.jpg">

                                                    <strong>Username</strong>
                                                    {{ $users->username }}
                                                </p>
                                                <span>
                                                    <strong>Name</strong>
                                                    {{ $users->full_name }}
                                                </span>
                                                <span>
                                                    <strong>Email</strong>
                                                    {{ $users->email }}
                                                </span>

                                            </div>
                                            <div class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#" href="#" aria-expanded="false">
                                                <p class="muted">

                                                    <br/>

                                                    <strong>Total Likes</strong>
                                                    {{ $users->likes_count }}
                                                </p>
                                                <span>
                                                    <strong>Total Comments</strong>
                                                    {{ $users->comments_count }}
                                                </span>
                                                <span>
                                                    <strong> User Type</strong>
                                                    @if($users->is_live == 0)         
                                                               <td>Public </td>
                                                            @else
                                                            <td>Private</td>        
                                                            @endif
                                                     
                                                </span>

                                            </div>

                                            <a  href="{{ url('/admin/user-profile/' . $users->id) }}">Profile</a>
                                        </h4>
                                    </div>
                                </div>                                        
                            </div>
                        </div>
                        @endforeach  
                        {{ $data->links() }}
                    </div>

                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!--- END QUICK SIDEBAR -->
</div>
@stop
