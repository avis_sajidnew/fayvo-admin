@extends('common_layouts.main_layouts')



@section('content')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN THEME PANEL -->
    <!-- END THEME PANEL -->
    <h1 class="page-title"> 
        <small>Database Statistics</small>
    </h1>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="index.html">Home</a>
                <i class="fa fa-angle-right"></i>
                <a href="index.html">Database Stats</a>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right filterz">
                <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> 
                    <i class="fa fa-filter filico"></i>

                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="index.html">
                            <i class="fa fa-th-large"></i> Dashboard Statistics</a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-database"></i> Database Statistics</a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-user-plus"></i> Login Statistics</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-like"></i>
                        <span class="caption-subject bold uppercase">Total Likes and Comments</span>
                    </div>
                    <div class="Src_to_from">
                        <div class="">
                            <label class="control-label mt5 pull-left mr10">Search Results</label>
                            <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                <input type="text" class="form-control" name="from" placeholder="From">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </div>
                            <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                <input type="text" class="form-control" name="from" placeholder="To">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </div>
                            <a href="http://www.highcharts.com/" class="btn green btn-outline ml15" target="_blank">Search</a>

                        </div>
                    </div><!--Src_to_from-->
                </div>
                <div class="portlet-body">
                    <div id="echarts_bar" style="height:500px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row stats2">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="268921">0</span>
                        </h3>
                        <small>Last 7 Days Trending</small>
                    </div>
                    <div class="icon">
                        <i class=" icon-speedometer"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-red-haze">
                            <span data-counter="counterup" data-value="89010">0</span>
                        </h3>
                        <small>Last 7 Days Trending Videos</small>
                    </div>
                    <div class="icon">
                        <i class=" icon-social-youtube"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-blue-sharp">
                            <span data-counter="counterup" data-value="213085">0</span>
                        </h3>
                        <small>Trending without Likes</small>
                    </div>
                    <div class="icon">
                        <i class="icon-like"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-blue-sharp">
                            <span data-counter="counterup" data-value="654431">0</span>
                        </h3>
                        <small>Trending without comments</small>
                    </div>
                    <div class="icon">
                        <i class=" icon-speech"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-bell"></i>
                        <span class="caption-subject bold uppercase">Active or Archived Notifications</span>
                    </div>
                    <div class="Src_to_from">
                        <div class="">
                            <label class="control-label mt5 pull-left mr10">Search Results</label>
                            <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                <input type="text" class="form-control" name="from" placeholder="From">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </div>
                            <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                <input type="text" class="form-control" name="from" placeholder="To">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </div>
                            <a href="http://www.highcharts.com/" class="btn green btn-outline ml15" target="_blank">Search</a>

                        </div>
                    </div><!--Src_to_from-->
                </div>
                <div class="portlet-body">
                    <div id="echarts_line" style="height:500px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row stats2">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-purple-soft">
                            <span data-counter="counterup" data-value="445087">0</span>
                        </h3>
                        <small>Active Notifications</small>
                    </div>
                    <div class="icon">
                        <i class="icon-bell"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-purple-soft">
                            <span data-counter="counterup" data-value="2410387">0</span>
                        </h3>
                        <small>Archived Notifications</small>
                    </div>
                    <div class="icon">
                        <i class="icon-grid"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop