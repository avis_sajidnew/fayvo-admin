<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Route::group(["prefix" => "admin", "middleware" => "before_auth"], function() {
    Route::get("login", ["uses" => "AdminController@loginPage"]);
    Route::post('postLogin', 'AdminController@postLogin')->name("postLogin");
    Route::get("/", ["uses" => "AdminController@loginPage"]);
});


Route::group(["prefix" => "admin", "middleware" => "admin_auth"], function() {
    Route::get("dashboard", ["uses" => "AdminController@dashboard"])->name('admin/dashboard');
    Route::get('logout', ["as" => "logout", 'uses' => 'AdminController@logout']);
    Route::get('reports', ["as" => "reports", 'uses' => 'AdminController@reports']);
    Route::post('reports', ["as" => "reports", 'uses' => 'AdminController@reports']);
    Route::get('user-recomendations', ["as" => "user-recomendations", 'uses' => 'AdminController@recomendations']);
    Route::get('read-message/{id}', ["as" => "read-message", 'uses' => 'AdminController@view_recomendation']);
    Route::get('users', ["as" => "users", 'uses' => 'UserController@user_listing']);
    Route::get('locations', ['as' => "locations", 'uses' => 'LocationController@locations']);
    Route::get('location/edit/{id}', ['as' => "editLocation", 'uses' => 'LocationController@edit_location']);
    Route::post('update-location', ['as' => "updateLocation", 'uses' => 'LocationController@update_location']);
    Route::get('user/complaints', ['as' => "userComplaints", 'uses' => 'ReporterController@all_reporting_listing']);
    Route::get('showException/{ex}', array('as' => 'showException', 'uses' => 'Controller@sendBackWithException'));
});

